#!/usr/bin/env python3

import sys
from cartographer_api.cartographer import Cartographer
from cartographer_api.helpers import get_credentials


def main():

    creds = get_credentials()
    cartographer = Cartographer(creds['user'], creds['key'], creds['host'])

    for item in cartographer.get_subnet_boundry_ips():
        print(item)

    return 0


if __name__ == "__main__":
    sys.exit(main())
