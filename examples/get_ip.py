#!/usr/bin/env python3

import sys
from cartographer_api.cartographer import Cartographer
from cartographer_api.helpers import get_credentials


def main():

    if len(sys.argv) != 2:
        sys.stderr.write("Usage: %s IP\n" % sys.argv[0])
        return 2
    else:
        ip = sys.argv[1]

    creds = get_credentials()
    cartographer = Cartographer(creds['user'], creds['key'], creds['host'])

    ip_obj = cartographer.get_ip(ip)
    print(ip_obj)
    print(ip_obj.json())

    return 0


if __name__ == "__main__":
    sys.exit(main())
