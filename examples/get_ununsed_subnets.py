#!/usr/bin/env python3

import sys
from cartographer_api.cartographer import Cartographer
from cartographer_api.helpers import get_credentials
import netaddr


def main():

    creds = get_credentials()
    cartographer = Cartographer(creds['user'], creds['key'], creds['host'])

    for possible_subnet in cartographer.get_subnets():
        print(possible_subnet.raw_data['cidr'])

    return 0


if __name__ == "__main__":
    sys.exit(main())
