#!/usr/bin/env python3

import sys
from cartographer_api.cartographer import Cartographer
from cartographer_api.helpers import get_credentials


def main():

    creds = get_credentials()
    cartographer = Cartographer(creds['user'], creds['key'], creds['host'])

    for ip_obj in cartographer.get_last_seen_ips(since='1 day ago'):
        print(ip_obj)

    return 0


if __name__ == "__main__":
    sys.exit(main())
