#!/usr/bin/env python3

import sys
import argparse
import dateparser
from cartographer_api.cartographer import Cartographer
from cartographer_api.helpers import get_credentials


def parse_args():
    parser = argparse.ArgumentParser(
        description='Lookup MAC based on IP in cartographer')
    parser.add_argument('mac', type=str, help='MAC Address to look up')
    parser.add_argument(
        '-w', '--when', type=str, default='Yesterday',
        help='Time to query for, use something dateparser can parse'
    )
    return parser.parse_args()


def main():

    args = parse_args()

    creds = get_credentials()
    cartographer = Cartographer(creds['user'], creds['key'], creds['host'])

    when = dateparser.parse(args.when)
    mac = cartographer.mac_to_ip_at(mac=args.mac, when=when)

    print(mac)

    return 0


if __name__ == "__main__":
    sys.exit(main())
