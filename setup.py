import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


with open('requirements.txt') as requirements_file:
    install_requirements = requirements_file.read().splitlines()

setup(
    name="python-cartographer",
    version="0.0.8",
    author="Jesse Bowling",
    author_email="jesse.bowling@duke.edu",
    description=("Python API wrapper for Cartographer"),
    keywords="cartographer",
    packages=find_packages(),
    install_requires=install_requirements,
    scripts=[
        'scripts/cquery.py'
    ],
    long_description=read('README.md'),
)
