#!/usr/bin/env python3
# encoding: utf-8

import configparser
import argparse
import json
import os

from cartographer_api.cartographer import Cartographer

__author__ = 'Jesse Bowling <jesse.bowling@duke.edu>'


def print_json(d):
    print(json.dumps(d, indent=4))


def query_ips(ips, c, func):
    answers = list()
    for ip in ips:
        d = getattr(c, func)(ip)
        answers.append(d)
    return answers

def query_macs(addresses, c):
    a = query_ips(addresses, c, 'ip_to_mac')
    d = dict()
    for item in a:
        for k,v in item.items():
            d[k] = v
    return d

def main():
    oparser = argparse.ArgumentParser(
        description='Query Cartographer for information',
        epilog='http://xkcd.com/353/')
    oparser.add_argument('-c', '--config',
                         required=False,
                         default=os.path.expanduser('~/.cartographer'),
                         help='Full path to a configuration file')
    oparser.add_argument('addresses',
                         nargs='*',
                         help='Addresses to query')
    oparser.add_argument('-i', '--ip',
                         action="store_true",
                         dest='ip',
                         default=False,
                         help='Treat as an IP search')
    oparser.add_argument('-r', '--reg',
                         action="store_true",
                         dest='dukereg',
                         default=False,
                         help='IP search and include Dukereg registration info')
    oparser.add_argument('-d', '--devices',
                         action="store_true",
                         dest='devices',
                         default=False,
                         help='Treat as a devices search; include search pattern to filter to (case insensitive)')
    oparser.add_argument('-s', '--subnet',
                         action="store_true",
                         dest='subnet',
                         default=False,
                         help='Treat as a subnet search; include search pattern to filter to (case insensitive)')
    oparser.add_argument('-v', '--vrf',
                         action="store_true",
                         dest='vrf',
                         default=False,
                         help='Treat as a vrf search; include search pattern to filter to (case insensitive)')
    oparser.add_argument('-l', '--vlan',
                         action="store_true",
                         dest='vlans',
                         default=False,
                         help='Treat as a vlan search; include search pattern to filter to (case insensitive)')
    oparser.add_argument('-m', '--mac',
                         action="store_true",
                         dest='mac',
                         default=False,
                         help='Retrieve an IP\'s Dukreg info and attempt to retrieve the MAC address')

    options = oparser.parse_args()

    config = configparser.ConfigParser()
    try:
        if os.path.isfile(options.config):
            config.read(options.config)
            user = config.get('client', 'user')
            key = config.get('client', 'key')
            host = config.get('client', 'host')
        else:
            print('Could not locate config at {0}'.format(options.config))
            exit(1)
    except (configparser.NoSectionError) as e:
        print('Unable to parse file at {0}'.format(options.config))
        print('Example file:')
        print('''
[client]
user = your_netid                   # Bare NetID to authenticate to Cartographer, i.e. 'jrb88'
key = api_key                       # (You can generate one here: https://cartographer.oit.duke.edu/api_info )
host = cartographer.oit.duke.edu    # One day there may be more...
              ''')
        exit(1)

    c = Cartographer(user, key, host)

    answers = dict()

    if options.ip:
        answers = query_ips(options.addresses, c, 'query_ip')
    elif options.dukereg:
        answers = query_ips(options.addresses, c, 'query_ip_dukereg')
    elif options.mac:
        answers = query_macs(options.addresses, c)
    elif options.vrf:
        answers = c.subnets_in_vrfs(options.addresses)
    elif options.subnet:
        answers = c.query_subnets(options.addresses, 'name')
    elif options.vlans:
        answers = c.query_subnets(options.addresses, 'vlans')
    elif options.devices:
        answers = c.query_devices(options.addresses)

    if answers:
        print_json(answers)


if __name__ == '__main__':
    main()
