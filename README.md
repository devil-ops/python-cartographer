## Getting Started

Make sure you have an API token set up with Cartographer.  Once you have the account, set up your credentials in ~/.cartographer like so:

```
[client]
user = your_netid
key = DO_NOT_SHARE
host = cartographer.oit.duke.edu
```

## Basic Usage

The library is in cartographer.py; a sample CLI is avilable with cquery.py.

```
usage: cquery.py [-h] [-c CONFIG] [-i] [-r] [-d] [-s] [-v] [-l]
                 [addresses [addresses ...]]

Query Cartographer for information

positional arguments:
  addresses             Addresses to query

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        Full path to a configuration file
  -i, --ip              Treat as an IP search
  -r, --reg             IP search and include Dukereg registration info
  -d, --devices         Treat as a devices search; include search pattern to
                        filter to (case insensitive)
  -s, --subnet          Treat as a subnet search; include search pattern to
                        filter to (case insensitive)
  -v, --vrf             Treat as a vrf search; include search pattern to
                        filter to (case insensitive)
  -l, --vlan            Treat as a vlan search; include search pattern to
                        filter to (case insensitive)
```

## API Documentation
```
https://cartographer.oit.duke.edu/api_info
```

Should the API change, or you have feature requests, please let me know!
