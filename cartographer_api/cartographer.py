#!/usr/bin/env python3
# encoding: utf-8

from collections import defaultdict
import requests
import dateparser
import requests_cache
import re
import sys
from cartographer_api.models import Subnet, Vrf, Device, Ip
from datetime import timedelta


class Cartographer(object):

    def __init__(self, user, key, host='cartographer.oit.duke.edu',
                 cache_lifetime=timedelta(hours=4)):

        self.session = requests.Session()
        self.session.auth = (user, key)
        self.baseurl = 'https://' + host + '/api/v1/'

        # Not sure this even works with sessions :(
        requests_cache.install_cache('cartographer_cache',
                                     expire_after=cache_lifetime)

    def __getattr__(self, item):
        return self.__dict__[item]

    def __call_route(self, route, params={}):
        try:
            r = self.session.get(route, params=params)
            return r.json()
        except Exception as e:
            raise e

    def query(self, *args, **kwargs):
        """
        Args:
          path    str: Path to append to baseurl
          params dict: Parameters to append to URL
          data   dict: Data to send along with request
          verb    str: Verb to use (default GET)
        Returns:
          Data in dict form
        """

        path = args[0]
        params = kwargs.get('params', {})
        data = kwargs.get('params', {})
        verb = kwargs.get('verb', 'GET')

        full_url = self.baseurl + path

        result = self.session.request(verb, full_url, params=params, data=data)
        if result.status_code != 200:
            sys.stderr.write("Error fulfilling request")
            raise Exception("CartographerRequestFailed")
        else:
            return result.json()

    def get_subnets(self, *args, **kwargs):
        """
        Returns:
          List of all Subnet objects
        """
        items = []
        for item in self.query('subnets/'):
            items.append(Subnet(self, **item))
        return items

    def get_subnet_boundry_ips(self, *args, **kwargs):
        """
        Returns:
          List of Reserved network IPs
        """
        items = []
        subnets = self.get_subnets()
        for subnet in subnets:
            ips = subnet.raw_data['cidr_details']['subnet_boundry_ips']
#           print(subnet, ips)
            items.extend(ips)
        return items

    def get_devices(self, *args, **kwargs):
        """
        Returns:
          List of all Device objects
        """
        items = []
        for item in self.query('devices/'):
            items.append(Device(self, **item))
        return items

    def get_ip(self, *args, **kwargs):
        """
        Args:
          ipaddress str: IPAddress to query
        Returns:
          Ip object
        """
        ipaddress = args[0]
        item = self.query('ip/%s' % ipaddress)
        item['name'] = ipaddress
        return Ip(self, **item)

    def get_last_seen_ips(self, *args, **kwargs):
        """
        Args:
          since str: dateparser date to retrieve
        Returns:
          Ip object list
        """
        since = kwargs.get('since', '1 day ago')
        since_obj = dateparser.parse(since)
        ips = []
        for ip, last_seen in self.query(
                'ip/last_seen?since=%s' % since_obj.strftime("%s")):
            if ip not in ips:
                ips.append(ip)
        ips.sort()
        return ips

    def get_vrf(self, *args, **kwargs):
        """
        Args:
          vrf_name str: Name of vrf to get
        Returns:
          Vrf object
        """

        vrf_name = args[0]
        item = self.query('vrfs/%s' % vrf_name)
        item['name'] = vrf_name
        return Vrf(self, **item)

    def get_vrfs(self, *args, **kwargs):
        """
        Returns:
          List of all Vrf objects
        """
        items = []
        vrf_names = self.query('vrfs/')
        for vrf_name in vrf_names:
            items.append(self.get_vrf(vrf_name))
        return items

    def __subnets(self):
        '''
        Call the subnet route and return the raw json object

        :return: list of dicts such as
        [
            {
                "dukereg_name": "10.128.4.0/24",
                "cidr": "10.128.4.0/24",
                "vrfs": [
                    "voip"
                    ]
            }
        ]
        '''

        route = self.baseurl + 'subnets/'
        d = self.__call_route(route)
        return d

    def __devices(self):
        '''
        Call the devices route and return the raw json object

        :return: list of dicts such as
        [
            {
                "name": "dukentp1",
                "url": "https://cartographer.oit.duke.edu/devices/1",
                "ips": [
                    "152.3.250.1"
                    ],
                "management_ip": "152.3.250.1",
                "serial_number": "FTX1041Z071",
                "model": "1811W",
                "type": "IOS",
                "id": 1
            }
        ]
        '''

        route = self.baseurl + 'devices/'
        d = self.__call_route(route)
        return d

    def __ip(self, ip):
        '''
        Call the ip route and return the raw json object
        :param ip: String representing IP address to query for
        :return: Dict containing information such as
        {
            "vlans": [
                        600
                    ],
            "subnet_cidr": "152.3.124.0/22",
            "subnet_name": "ATC Strickland 1 (152.3.124.0/22)",
            "vrfs": [
                    "campus"
                    ],
            "owning_device_names": []
        }

        '''
        route = self.baseurl + 'ip/' + ip
        d = self.__call_route(route)
        d['ip'] = ip
        return d

    def __dukereg(self, ip):
        '''
        Call the Dukereg route and return the raw object
        :param ip: String representing IP address to query for
        :return: Dict containing the information such as
        {
            "all_names": [
                "oldartem.wired.duke.edu"
            ],
            "lease_end": "2014-11-11 11:53:58.0",
            "lease_start": "2014-11-05 11:53:58.0",
            "mac_info": {
                "address": "28:D2:44:00:C8:DA",
                "description": "Lenovo y500",
                "exp_date": "2016-07-06 00:00:00.0",
                "last_seen_dhcp": "2016-01-06 04:41:13.0",
                "operating_system": "Fedora/Ubuntu/Debian",
                "reg_by": "jrb88",
                "reg_date": "2014-08-11 12:32:43.0",
                "user_name": "jrb88"
            },
            "type": "DHCP Reserved",
            "url": "https://dukereg-admin.oit.duke.edu/app?component=$Border&page=Home&service=direct&sp=Svalue=IP4Addr:1894402"
        }

        '''
        route = self.baseurl + 'ip/' + ip + '/dukereg_info'
        d = self.__call_route(route)
        return d

    def __filter_dict(self, data, fstrings):
        '''
        Given a dictionary, return a new dictionary where the keys match the fstring given
        :param data: dictionary to search
        :param fstring: lists of string representing a regex to search keys with
        :return: dictionary containing only keys that match the fstring
        '''
        d = dict()
        for key in data:
            for fstring in fstrings:
                if re.search(fstring, str(key), re.IGNORECASE):
                    d[key] = data[key]
        return d

    def __filter_list_of_dicts(self, data, fstrings, kname=None):
        '''
        Give a list of dicts, a list of filters, and an optional key name, return a filtered list of dicts

        :param data: List of dicts to be filtered
        :param fstrings: lists of string representing a regex to search keys with
        :param kname: If specified, search only this key with filters, otherwise filter all keys
        :return: filtered list of dicts
        '''
        d = list()
        if kname:
            for item in data:
                for fstring in fstrings:
                    if re.search(fstring, str(item[kname]), re.IGNORECASE):
                        d.append(item)
        else:
            for item in data:
                for fstring in fstrings:
                    for key in item:
                        if re.search(fstring, str(item[key]), re.IGNORECASE):
                            d.append(item)
        return d

    def query_ip(self, ip):
        d = self.__ip(ip)
        return d

    def query_dukereg(self, ip):
        d = self.__dukereg(ip)
        return d

    def mac_to_ip_at(self, *args, **kwargs):
        """
        Arguments:
          mac  str: MAC Address to look up
          when datetime: Time to look up IP from
        Returns:
          ipaddress str: IP Address at a given time
        """
        mac = kwargs.get('mac')
        when = kwargs.get('when')

        results = self.__call_route(
           self.baseurl + 'ip/by_mac_address/%s?at=%s' % (
               mac, when.strftime("%s")))

        arp = results['arp']
        if len(arp) == 0:
            sys.stderr.write("No arp found for that")
            raise Exception("NoArpFound")
        elif len(arp) > 1:
            sys.stderr.write("Found more than 1 arp for that")
            raise Exception("TooManyArpFound")
        else:
            return arp[0]

    def ip_to_mac_at(self, *args, **kwargs):
        """

        Arguments:
          ipaddress      str: IPAddress to look up
          when      datetime: Time to look up IP from
        Returns:
          mac_address    str: MAC Address at a given time
        """
        ipaddress = kwargs.get('ipaddress')
        when = kwargs.get('when')

        results = self.__call_route(
           self.baseurl + 'mac_addresses/by_ip/%s?at=%s' % (
               ipaddress, when.strftime("%s")))

        url = self.baseurl + 'mac_addresses/by_ip/%s?at=%s' % (ipaddress, when.strftime("%s"))

        arp = results['arp']
        if len(arp) == 0:
            sys.stderr.write("No arp found for that")
            raise Exception("NoArpFound")
        elif len(arp) > 1:
            sys.stderr.write("Found more than 1 arp for that")
            raise Exception("TooManyArpFound")
        else:
            return arp[0]

    def ip_to_mac(self, ip):
        d = self.__dukereg(ip)
        if d.get('mac_info'):
            a = d['mac_info']
            if a.get('address'):
                return {ip: a['address']}
            else:
                return {ip: None}
        else:
            return {ip: None}

    def query_ip_dukereg(self, ip):
        '''
        Given an string IP, return both the IP name and the Dukereg information in a single dictionary
        :param ip: String representing the IP
        :return: dictionary containing IP and Dukereg information
        '''
        a = self.__ip(ip)
        b = self.__dukereg(ip)
        d = a.copy()
        d.update(b)
        return d

    def query_devices(self, filters=None):
        '''
        Call the devices route, and return a dictionary of devices where the name key matches the filter
        :param filters: string representing regex to filter list by
        :return: dictionary containing matching devices such as
        {
            "id": 1121,
            "name": "blue-university-pl-1",
            "management_ip": "10.140.27.67",
            "serial_number": "CAT0917Y12T",
            "model": "WS-C3750-24P",
            "type": "IOS",
            "function": "Switch",
            "url": "https://cartographer.oit.duke.edu/devices/1121",
            "ips": [
                "10.140.27.67"
            ],
            "mac_addresses": [
                "00:13:C4:C4:59:03",
                "00:13:C4:C4:59:41",
                ...
            ],
            "last_queried": "2018-01-17T21:14:39.000-05:00",
            "software_version": "12.2(44)SE5"
        }

        '''
        d = self.__devices()
        if filters:
            d = self.__filter_list_of_dicts(d, filters, 'name')
        return d

    def query_subnets(self, filters=None, kname=None):
        d = self.__subnets()
        if filters:
            d = self.__filter_list_of_dicts(d, filters, kname)
        return d

    def subnets_in_vrfs(self, filters=None):
        '''
        Format subnet data into a dictionary of lists
        :param filter: string representing regex to filter list by
        :return: dictionary of lists such as
        {
            "math": [
                    "152.3.25.0/24",
                    "152.3.26.0/24",
                    "152.3.73.0/27",
                    "152.3.73.32/27"
                    ]
        }
        '''

        vrfs = defaultdict(list)
        d = self.__subnets()
        for item in d:
            if len(item['vrfs']) == 1:
                vrfs[item['vrfs'][0]].append(item['cidr'])
            elif len(item['vrfs']) == 0:
                vrfs[u'non-vrf'].append(item['cidr'])
            else:
                for v in item['vrfs']:
                    vrfs[v].append(item['cidr'])
        if filters:
            vrfs = self.__filter_dict(vrfs, filters)
        return vrfs
