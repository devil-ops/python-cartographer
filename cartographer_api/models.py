import json
import netaddr


class Subnet(object):
    """
    Object representing a 'Subnet' in Cartographer
    """

    def __init__(self, api, *args, **kwargs):
        self.raw_data = kwargs
        self.api = api
        self.expand_cidr()

    def expand_cidr(self, *args, **kwargs):
        """
        Add some information about the CIDR range to this object
        """
        cidr_obj = netaddr.IPNetwork(self.raw_data['cidr'])
        cidr = {}
        cidr['size'] = cidr_obj.size
        cidr['broadcast'] = str(cidr_obj.broadcast)
        cidr['is_private'] = cidr_obj.is_private()
        cidr['netmask'] = str(cidr_obj.netmask)

        # These are IPs that we as Duke consider reserved
        cidr['subnet_boundry_ips'] = []
        if cidr['size'] > 0:
            cidr['subnet_boundry_ips'].append("%s" % cidr_obj[0])
            cidr['subnet_boundry_ips'].append("%s" % cidr_obj[-1])

        self.raw_data['cidr_details'] = cidr

    def expand_vrfs(self, *args, **kwargs):
        """
        Loops through vrf name list, and make them objects
        """
        vrf_names = self.raw_data['vrfs']
        real_vrfs = []
        for vrf_name in vrf_names:
            real_vrfs.append(Vrf(self, self.api.get_vrf(vrf_name)))
        self.raw_data['vrf_objects'] = real_vrfs

    def __getattr__(self, key):
        return self.raw_data[key]

    def __repr__(self):
        return u"<Cartographer.Subnet.%s object>" % self.raw_data['cidr']

    def json(self):
        return(json.dumps(self.raw_data))


class Vrf(object):
    """
    Object representing a 'Vrf' in Cartographer
    """

    def __init__(self, api, *args, **kwargs):
        self.raw_data = kwargs
        self.api = api

    def __getattr__(self, key):
        return self.raw_data[key]

    def __repr__(self):
        return u"<Cartographer.Vrf.%s object>" % self.raw_data['name']

    def json(self):
        return(json.dumps(self.raw_data))


class Device(object):
    """
    Object representing a 'Device' in Cartographer
    """

    def __init__(self, api, *args, **kwargs):
        self.raw_data = kwargs
        self.api = api

    def __getattr__(self, key):
        return self.raw_data[key]

    def __repr__(self):
        return u"<Cartographer.Vrf.%s object>" % self.raw_data['name']

    def json(self):
        return(json.dumps(self.raw_data))


class Ip(object):
    """
    Object representing a 'Ip' in Cartographer
    """

    def __init__(self, api, *args, **kwargs):
        self.raw_data = kwargs
        self.api = api

    def __getattr__(self, key):
        return self.raw_data[key]

    def __repr__(self):
        return u"<Cartographer.Ip.%s object>" % self.raw_data['name']

    def json(self):
        return(json.dumps(self.raw_data))
