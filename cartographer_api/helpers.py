import os
import configparser


def get_credentials(*args, **kwargs):
    """
    Pull in credentials from ~/.cartographer

    Format of file should be

    ```
    [client]
    user = your_netid
    key = DO_NOT_SHARE
    host = cartographer.oit.duke.edu
    ```

    Args:
      credentials_file (str): Credential file to use (~/.cartographer)

    Returns:
      dict: Values from given credentials file
    """

    credentials_file = kwargs.get('credentials_file', '~/.cartographer')
    config_file = os.path.expanduser(credentials_file)

    config = configparser.ConfigParser()

    config.read(config_file)
    user = config.get('client', 'user')
    key = config.get('client', 'key')
    host = config.get('client', 'host')

    return {
        'user': user,
        'key': key,
        'host': host
    }
